/**
 *  LOF JSliderNews
 *  Update: 2012/02/09
 */


Summary
-------
This project provide a new content type to display the fancy slides. 
You can create your slide from node adding page. We start this project 
is because many of carousel project doesn't provide "content" area for 
display some text in carousel.

This module based on "Land Of Coder Lof" JSliderNews JQuery plugin.
From: http://landofcoder.com/
Demo: http://landofcoder.com/demo/jquery/lofslidernews/


Basic Functionality
-------------------
Here is feature list of this module:

* Show fancy slider
* Create a new content type which name is "Carousel"
* Configuration this slider animation effect, quantity, height and many...
* Provide a Block use
* Custom this style sheet


Requirements and Dependencies
-----------------------------
 * Content                            --- http://drupal.org/project/cck
 * FileField                          --- http://drupal.org/project/filefield
 * ImageAPI (ImageAPI, ImageAPI GD2)  --- http://drupal.org/project/imageapi
 * ImageField                         --- http://drupal.org/project/imagefield
 * ImageCache                         --- http://drupal.org/project/imagecache
 * Link                               --- http://drupal.org/project/link

 
Installation and Settings
-------------------------
TO successful completion of this installation. 

First, go (http://landofcoder.com/) website, and click menu 
(DOWNLOAD > Free Download) than you can see "Jquery Plugin" 
folder, there have Lof JSliderNews Plugin you can direct download.
Download Link:
(http://landofcoder.com/index.php?option=com_jdownloads&Itemid=71&task=finish&cid=9&catid=5)

Second, Decompress "lofslidernews.zip" and copy this "lofslidernews" folder to Lof 
JSliderNews module folder root. 

Third, you must to install above "Dependencies" module, and according to the 
following steps:

1) Take the "lof_jslidernews" folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).

3) Create a new content "Carousel" and enter (/node/add/carousel). Every this 
   content will be display in the block.
   
4) Settinga and onfigure this module (/admin/settings/lof_jslidernews).

5) If you want to custom this block style sheet, you can create "custom.css" file 
   into this module folder root (ex. /sites/all/modules/lof_jslidernews). When this 
   module has be updated, you can copy your style sheet into the same place.


Author
------
  Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)

  If you use this module, find it useful, and want to send the author
  a thank or you note, feel free to contact me.

  The author can also be contacted for paid customizations of this and other modules.