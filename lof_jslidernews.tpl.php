<?php
/**
 * @file lof_jslidernews.tpl.php
 * Default theme implementation for displaying a carousel theme layout.
 *
 * Available variables:
 * - $lof_thumb: Array of thumbnail of carousel.
 * - $lof_img: Array of keyed image with link.
 * - $lof_content: Array of content associate same key with $lof_img
 * - $lof_width: Absolute with value defined for wrapper of carousel
 * - $lof_height: Absolute value defined for wrapper of carousel
 *
 * @see template_preprocess_lof_jslidernews()
 */
global $base_url;
?>
<?php if(!empty($lof_img)){ ?>
<!------------------------------------- THE CONTENT ------------------------------------------------->
<div id="jslidernews" class="lof-slidecontent" style="width:<?php print $lof_width ?>px;height:<?php print $lof_height ?>px">
	<div class="preload"><div></div></div>
    		 <!-- MAIN CONTENT --> 
              <div class="main-slider-content" style="width:<?php print $lof_width ?>px;height:<?php print $lof_height ?>px">
                <ul class="sliders-wrap-inner">
                  <?php foreach($lof_img as $k => $img){ ?>
                  <li>
                    <?php print $img; ?>
                    <div class="slider-description">
                      <h1 class="title"><?php print $lof_title[$k]; ?></h1>
                      <p class="body"><?php print $lof_content[$k]; ?>
                        <span class="readmore-wrap"><?php print $lof_readmore[$k]; ?></span>
                      </p>
                    </div>
                  </li>
                  <?php } ?>
                </ul>  	
            </div>
 		   <!-- END MAIN CONTENT --> 
           <!-- NAVIGATOR -->
           	<div class="navigator-content">
                  <div class="button-next">Next</div>
                  <div class="navigator-wrapper">
                        <ul class="navigator-wrap-inner">
                           <?php print '<li>'.implode('</li><li>', $lof_thumb). '</li>'; ?>  		
                        </ul>
                  </div>
                  <div  class="button-previous">Previous</div>
             </div> 
          <!----------------- END OF NAVIGATOR --------------------->
          <!-- BUTTON PLAY-STOP -->
          <div class="button-control"><span></span></div>
           <!-- END OF BUTTON PLAY-STOP -->
          
 </div> 


<!------------------------------------- END OF THE CONTENT ------------------------------------------------->
<script type="text/javascript">
 $(document).ready( function(){	
		// buttons for next and previous item						 
		var buttons = { previous:$('#jslidernews .button-previous') ,
						next:$('#jslidernews .button-next') };			
		 $('#jslidernews').lofJSidernews({
        mainWidth:<?php print $lof_width ?>,
        navigatorWidth:<?php print $lof_navigator_width+7 ?>,
        navigatorHeight:<?php print $lof_navigator_height+7 ?>,
        maxItemDisplay:<?php print $lof_max_item_display ?>,
        direction:"<?php print $lof_direction ?>",
        interval:<?php print $lof_interval ?>,
        duration:<?php print $lof_duration ?>,
        easing:"<?php print $lof_easing ?>",
        navPosition:"<?php print $lof_nav_position ?>",
        auto:<?php print $lof_auto ?>,
        startItem:<?php print $lof_start_item-1 ?>,
        navigatorEvent:"<?php print $lof_navigator_event ?>",
        buttons: buttons 
      });	
	});
</script>
<?php 
  } 
  else if($_GET['q'] == 'admin/settings/lof_jslidernews') { 
    drupal_set_message("<a href='{$base_url}/node/add/carousel'>Create Carousel Now</a>", 'warning');
  }
  else if($_GET['q'] == 'node/add/carousel') {
    drupal_set_message("<a href='{$base_url}/admin/settings/lof_jslidernews'>Take a setting Now</a>", 'warning');
  }
?>